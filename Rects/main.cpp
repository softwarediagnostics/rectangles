// Rects - calculates rectangle intersections. 
// Copyright (c) 2017 by Dmitry Vostokov
// Version 1.1.001

#include "stdafx.h"
#include "Rects.h"

const size_t MAX_READ = 1000; // The maximum number of rectangles to read from the file

int main(int argc, char *argv[])
{
	// Display a logo if a file name is missing. Ignore the rest of arguments if the file name is given.

	if (argc < 2)
	{
		cout << "Usage: Rects JSON_file" << endl;
		return 0;
	}

	ifstream file(argv[1]);

	if (!file)
	{
		cerr << "Couldn't open the file: " << argv[1] << endl;
		return -1;
	}

	RECTANGLES rects;

	// Read a file. Populate the internal representation with input data.

	if (!rects.ReadInputData(file, MAX_READ))
	{
		cerr << "Errors while reading the file: " << argv[1] << endl;
		return -1;
	}

	// Write input data to concole.

	rects.WriteInputData();

	// Process input rectangles.

	rects.ProcessData();

	// write results to concole.

	rects.WriteResults();

	return 0;
}
