#pragma once

#include "stdafx.h"

using namespace std;

struct RECTINFO // Plain old struct
{
	int m_left, m_top; // "X, Y integer coordinates for one corner", "Rectangles are oriented with X,Y at the top left corner"
	unsigned int m_width, m_height; // "Width and height are always positive integers" 
	size_t m_src1, m_src2; // 0-based indexes of rectangles the rectangle was combined from.
	RECT m_winRect; // To use Windows API for rectangle arithmetic and checks. Can be replaced by own functions later if necessary.
};

class RECTANGLES
{
private:
	vector<RECTINFO> m_rects;

public:

	// we allow default constructor, copy constructor, destructor, and copy assignment operator

	bool ReadInputData(istream&, size_t); // read up to size_t rectangles
	void WriteInputData();
	void ProcessData();
	void WriteResults();
	
private:
	bool CheckIntersection(RECTINFO&, size_t, size_t);
	void BuildSortedIndexes(set<size_t>&, RECTINFO&);
};

ostream& operator<<(ostream&, const RECTINFO&);
