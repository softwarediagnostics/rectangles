#include "stdafx.h"
#include "Rects.h"

// 
// We do very lightweight parsing of input stream. Only checking for "rects" and then matching "x", "y", "w", "h" in any order inside a 4-tuple. 
// Input is case sensitive.
//
// Returns true if rects is populated and there were no input and parsing errors.
//
bool RECTANGLES::ReadInputData(istream& file, size_t maxRects) 
{
	string line;
	const string delimeters(" \t,\":{}[]");
	bool isRectsSection = false, isX = false, isY = false, isW = false, isH = false;
	RECTINFO rect;
	size_t rectConstructionLevel = 0;

	m_rects.reserve(2 * maxRects); // Reserve more as intersection rectangles will be added to the same vector.

	while (getline(file, line) && m_rects.size() < maxRects)
	{
#ifdef _DEBUG
		cout << line << endl;
#endif
		string::size_type startPos = line.find_first_not_of(delimeters), endPos;

		while (startPos != string::npos)
		{
			if ((endPos = line.find_first_of(delimeters, startPos)) == string::npos)
			{
				endPos = line.length();
			}

			string token = line.substr(startPos, endPos - startPos);

#ifdef _DEBUG
			cout << token << endl;
#endif

			if (token == "rects")
			{
				isRectsSection = true;
			}
			else if (isRectsSection)
			{
				if (token == "x")
				{
					isX = true;
				}
				else if (token == "y")
				{
					isY = true;
				}
				else if (token == "w")
				{
					isW = true;
				}
				else if (token == "h")
				{
					isH = true;
				}
				else
				{
					++rectConstructionLevel;
					try
					{
						if (isX)
						{
							rect.m_left = stoi(token);
							isX = false;
						}
						else if (isY)
						{
							rect.m_top = stoi(token);
							isY = false;
						}
						else if (isW)
						{
							rect.m_width = stoul(token);
							isW = false;
						}
						else if (isH)
						{
							rect.m_height = stoul(token);
							isH = false;
						}
					}
					catch (const exception& e)
					{
						cerr << "Exception: " << e.what() << " Line: " << line << " Token: " << token << endl;
						return false;
					}

					if (rectConstructionLevel == 4)
					{
						rect.m_src1 = rect.m_src2 = m_rects.size();
						m_rects.push_back(rect);
						rectConstructionLevel = 0;

						if (m_rects.size() == maxRects)
						{
							break;
						}
					}
				}
			}
			else
			{
				cerr << "Unknown file format. Line: " << line << " Token: " << token << endl;
				return false;
			}

			startPos = line.find_first_not_of(delimeters, endPos);
		}
	}

	return true;
}

void RECTANGLES::WriteInputData()
{
	cout << "Input:" << endl;

	for (auto rect : m_rects)
	{
		cout << '\t' << rect.m_src1 + 1 << ": Rectangle at " << rect << '.' << endl;
	}
}

//
// The algorithm processes rectangles between startInput and endInput vector indexes (which initially consist of input rectangles) 
//   and adds intersections to the same vector, and updates startInput and endInput, and then repeats until there are no more intersections.
//
// Performance note: Indexes are sliding and shrinking so the performance is better than O(n^3).
//

void RECTANGLES::ProcessData()
{
	size_t inputSize = m_rects.size();
	size_t startInput = 0, endInput = inputSize;

	do {
		inputSize = endInput;

		for (size_t pos1 = startInput; pos1 < inputSize; ++pos1)
		{
			for (size_t pos2 = pos1 + 1; pos2 < inputSize; ++pos2)
			{
				RECTINFO rectNew = { 0 };

				if (CheckIntersection(rectNew, pos1, pos2))
				{
					++endInput;
					m_rects.push_back(rectNew);
#ifdef _DEBUG
					cout << "Added: " << rectNew << " between " << rectNew.m_src1 + 1 << " and " << rectNew.m_src2 + 1 << endl;
#endif
				}
			}
		}

		startInput = inputSize;
	} while (inputSize < endInput);
}

// 
// Checks if rectangles at vector src1 and src2 positions intersect and fills the new rectangle.
//
bool RECTANGLES::CheckIntersection(RECTINFO& dstRect, size_t src1, size_t src2)
{
	// fill Windows rects
	RECTINFO& rect1 = m_rects[src1], rect2 = m_rects[src2];

	if ((rect1.m_src1 != rect1.m_src2) && (rect1.m_src2 != rect2.m_src1))
	{ 
		return false; // TODO: prove
	}
	
	::SetRect(&rect1.m_winRect, rect1.m_left, rect1.m_top, rect1.m_left + rect1.m_width, rect1.m_top + rect1.m_height);
	::SetRect(&rect2.m_winRect, rect2.m_left, rect2.m_top, rect2.m_left + rect2.m_width, rect2.m_top + rect2.m_height);

	RECT winDst = { 0 };
	if (::IntersectRect(&winDst, &rect1.m_winRect, &rect2.m_winRect))
	{
		dstRect.m_left = winDst.left;
		dstRect.m_top = winDst.top;
		dstRect.m_width = winDst.right - winDst.left;
		dstRect.m_height = winDst.bottom - winDst.top;

		dstRect.m_src1 = src1;
		dstRect.m_src2 = src2;

		return true;
	}

	return false;
}

//
// Builds the sorted sequence of rectangle numbers participated in a rectangle intersection. 
// Any RECTINFO rectangle contains indexes of 2 source intersecting rectangles.    
//
void RECTANGLES::BuildSortedIndexes(set<size_t>& indexes, RECTINFO& rect)
{
	if (rect.m_src1 == rect.m_src2) // input rectangle
	{
		indexes.insert(rect.m_src1);
		return;
	}

	BuildSortedIndexes(indexes, m_rects[rect.m_src1]);
	BuildSortedIndexes(indexes, m_rects[rect.m_src2]);
}

void RECTANGLES::WriteResults()
{
	cout << endl << "Intersections:" << endl;

	for (auto rect : m_rects)
	{
		if (rect.m_src1 == rect.m_src2) continue; // These are input rectangles.

		cout << "\tBetween rectangle ";

		// build sorted indexes

		set<size_t> indexes;
		BuildSortedIndexes(indexes, rect);

		size_t sz = indexes.size();
		for (auto pos : indexes)
		{
			cout << pos + 1 << ((sz > 2) ? ", " : ((sz == 2) ? " and " : " "));
			sz--;
		}

		cout << "at " << rect << '.' << endl;
	}
}

ostream& operator<<(ostream& os, const RECTINFO& ri)
{
	return os << '(' << ri.m_left << ',' << ri.m_top << "), w=" << ri.m_width << ", h=" << ri.m_height;
}