# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a test program that calculates rectangle intersections. Rectangles.vsd Visio file provides a graphical example.

The following input file in JSON format

    {
     "rects": [
      {"x": 100, "y": 100, "w": 250, "h": 80 },
      {"x": 120, "y": 200, "w": 250, "h": 150 },
      {"x": 140, "y": 160, "w": 250, "h": 100 },
      {"x": 160, "y": 140, "w": 350, "h": 190 }
     ]
    }

results in the output:

    Input:
            1: Rectangle at (100,100), w=250, h=80.
            2: Rectangle at (120,200), w=250, h=150.
            3: Rectangle at (140,160), w=250, h=100.
            4: Rectangle at (160,140), w=350, h=190.

    Intersections:
            Between rectangle 1 and 3 at (140,160), w=210, h=20.
            Between rectangle 1 and 4 at (160,140), w=190, h=40.
            Between rectangle 2 and 3 at (140,200), w=230, h=60.
            Between rectangle 2 and 4 at (160,200), w=210, h=130.
            Between rectangle 3 and 4 at (160,160), w=230, h=100.
            Between rectangle 1, 3 and 4 at (160,160), w=190, h=20.
            Between rectangle 2, 3 and 4 at (160,200), w=210, h=60.
		
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Download Visual Studio Community 2017 
During installation choose Desktop development with C++ with the following features:

    VC++ 2017 v141 toolset (x86,x64)
    Windows 10 SDK (10.0.15063.0) for Desktop C++ x86 and x64

After installation load Rects.sln solution. Choose Release x64 build. 

Test file rects.json is located under \Rects folder

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact